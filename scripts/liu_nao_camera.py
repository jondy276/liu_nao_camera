#!/usr/bin/env python

"""
Copyright (c) 2014, Jon Dybeck
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of [project] nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# Import ROS.
import rospy
from sensor_msgs.msg import Image

# Import NaoQi.
from naoqi import ALProxy
import vision_definitions

class LiuNaoCameraNode(object):
    """Fetches images from NaoQi and publishes them in ROS."""

    def __init__(self):
        """Initializes a ROS node and a NaoQi proxy."""
        # Initialize ros node.
        self.initialize_ros_node()
        # Get configurable parameters from ROS.
        self._naoqi_address = rospy.get_param("~naoqi_address")
        self._naoqi_port = rospy.get_param("~naoqi_port")
        self._fps = rospy.get_param("~fps")
        self._is_local = rospy.get_param("~is_local")

        # Define camera constants.
        # See https://community.aldebaran-robotics.com/doc/1-14/naoqi/vision/alvideodevice-indepth.html
        self._resolution = vision_definitions.kVGA
        self._color_space = vision_definitions.kBGRColorSpace

        self._subscribed = False

        self.connect_proxy()
        self.subscribe_camera()

    def initialize_ros_node(self):
        """Initializes ROS, creates the node and publisher."""
        rospy.init_node('liu_nao_camera_node', anonymous=True)
        self._image_publisher = rospy.Publisher('~image', Image)

    def connect_proxy(self):
        """Connect NaoQi proxy."""

        # Connect NaoQi proxy.
        rospy.loginfo("Creating NaoQi proxy to: " +
                      str(self._naoqi_address) + ":" + str(self._naoqi_port))
        self._proxy = ALProxy("ALVideoDevice",
                              self._naoqi_address, self._naoqi_port)

    def configure_camera(self):
        """Configures the camera parameters."""
        # Disable auto exposure
        self._proxy.setCameraParameter(self._proxy_id, vision_definitions.kCameraAutoExpositionID, 0)
        # Disable auto white balance
        self._proxy.setCameraParameter(self._proxy_id, vision_definitions.kCameraAutoWhiteBalanceID, 0)

    def restore_camera_defaults(self):
        """Restores the default configuration of the camera parameters."""
        # Reset default for auto exposure
        self._proxy.setCameraParameterToDefault(self._proxy_id, vision_definitions.kCameraAutoExpositionID)
        # Reset default for auto white balance
        self._proxy.setCameraParameterToDefault(self._proxy_id, vision_definitions.kCameraAutoWhiteBalanceID)

    def subscribe_camera(self):
        """Subscribe to Nao camera."""
        # Subscribe to camera.
        if not self._subscribed:
            rospy.loginfo("Subscribing to NaoQi camera.");
            self._proxy_id = self._proxy.subscribe(rospy.get_name(), self._resolution,
                                                   self._color_space, self._fps)
            self.configure_camera()
            self._subscribed = True

    def unsubscribe_camera(self):
        """Unsubscribe from camera."""
        if self._subscribed:
            rospy.loginfo("Unsubscribing from NaoQi camera.")
            self.restore_camera_defaults()
            self._proxy.unsubscribe(self._proxy_id)
            self._subscribed = False

    def get_image(self):
        """Get image from camera."""
        if self._is_local:
            return self._proxy.getImageLocal(self._proxy_id)
        else:
            return self._proxy.getImageRemote(self._proxy_id)

    def forward_image(self):
        """Get image from NaoQi and publish in ROS."""

        naoqi_image = self.get_image()

        # Unpack the integer array.
        # See https://community.aldebaran-robotics.com/doc/1-14/naoqi/vision/alvideodevice-tuto.html
        # For format of the integer array in naoqi_image.
        width = naoqi_image[0]
        height = naoqi_image[1]
        layers = naoqi_image[2]
        color_space = naoqi_image[3]
        seconds = naoqi_image[4]
        micro_seconds = naoqi_image[5]
        image_bytes = naoqi_image[6]

        # Construct ROS image message.
        # https://github.com/ros/common_msgs/blob/indigo-devel/sensor_msgs/msg/Image.msg
        ros_image = Image()
        # Initialize header.
        ros_image.header.stamp.secs = seconds
        ros_image.header.stamp.nsecs = micro_seconds * 1000
        ros_image.header.frame_id = "0" # No frame of referense.
        # Initialize image.
        ros_image.height = height
        ros_image.width = width
        # This must be the same as self._color_space.
        # See https://github.com/ros/common_msgs/blob/indigo-devel/sensor_msgs/include/sensor_msgs/image_encodings.h
        ros_image.encoding = "bgr8"
        ros_image.is_bigendian = 0
        ros_image.step = width * layers # Number of bytes per row.
        ros_image.data = image_bytes

        # Publish image.
        self._image_publisher.publish(ros_image)

        # Release image (needed in Python?).
        self._proxy.releaseImage(self._proxy_id)

    def has_subscribers(self):
        """Checks if there are subscribers to the ROS image topic."""
        return self._image_publisher.get_num_connections()

    def run(self):
        """Run the node until ROS shuts down."""
        rospy.loginfo("Running idle loop.")
        r = rospy.Rate(self._fps)
        while not rospy.is_shutdown():
            if self.has_subscribers():
                self.subscribe_camera()
                self.forward_image()
            else:
                self.unsubscribe_camera()
            r.sleep()

if __name__ == "__main__":
    node = None

    try:
        node = LiuNaoCameraNode()
    except rospy.ROSInterruptException:
        # Prevents crashing if interrupted.
        # Reduces the amount of unrelated errors.
        quit()

    try:
        node.run()
    finally:
        # Make sure that the subscription is removed.
        node.unsubscribe_camera()
